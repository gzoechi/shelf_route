// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

/// This library is for developers that wish to extend the RouterImpl
library shelf_route.extend;

export 'shelf_route.dart';
export 'src/router_impl.dart';
